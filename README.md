# SpringBoot && MongoDB
Sample application for learning 😎


#### Config properties

    touch ./src/main/resources/application.yml

in **application.yml**

    spring:
        data:
            mongodb:
                uri: mongodb+srv://<user>:<password>@<host>/store_db?retryWrites=true&w=majority
                database: store_db
                auto-index-creation: true