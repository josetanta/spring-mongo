package ia.system.startmongo.infrastructure.adapaters.output.persistance.mapper;

/**
 * @param <M> Model or domain
 * @param <E> Entity or persistence
 */
public interface PersistenceMapper<M, E> {
    E toEntity(M model);

    M toModel(E entity);
}
