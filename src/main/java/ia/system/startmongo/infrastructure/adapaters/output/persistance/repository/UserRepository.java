package ia.system.startmongo.infrastructure.adapaters.output.persistance.repository;

import ia.system.startmongo.infrastructure.adapaters.output.persistance.entity.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<UserEntity, String> {
}
