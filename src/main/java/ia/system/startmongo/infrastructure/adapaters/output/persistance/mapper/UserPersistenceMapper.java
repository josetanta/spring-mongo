package ia.system.startmongo.infrastructure.adapaters.output.persistance.mapper;

import ia.system.startmongo.domain.models.User;
import ia.system.startmongo.infrastructure.adapaters.output.persistance.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@RequiredArgsConstructor
public class UserPersistenceMapper implements PersistenceMapper<User, UserEntity> {

    private final TaskPersistenceMapper taskPersistenceMapper;

    @Override
    public UserEntity toEntity(User model) {
        return UserEntity
            .builder()
            .email(model.getEmail())
            .username(model.getUsername())
            .taskEntities(new ArrayList<>())
            .build();
    }

    @Override
    public User toModel(UserEntity entity) {
        return User.builder()
            .email(entity.getEmail())
            .username(entity.getUsername())
            .id(entity.getId())
            .tasks(entity.getTaskEntities().stream()
                .map(taskPersistenceMapper::toModel)
                .toList()
            )
            .build();
    }
}
