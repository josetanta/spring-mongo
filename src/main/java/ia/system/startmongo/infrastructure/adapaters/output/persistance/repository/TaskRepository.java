package ia.system.startmongo.infrastructure.adapaters.output.persistance.repository;

import ia.system.startmongo.infrastructure.adapaters.output.persistance.entity.TaskEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends MongoRepository<TaskEntity, String> {

    @Query("{ 'ownerId': '?0' }")
    List<TaskEntity> findAllByOwnerId(String ownerId);
}
