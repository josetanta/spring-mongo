package ia.system.startmongo.infrastructure.adapaters.input.rest;

import ia.system.startmongo.domain.services.TaskService;
import ia.system.startmongo.infrastructure.adapaters.input.rest.dto.request.TaskCreateDTO;
import ia.system.startmongo.infrastructure.adapaters.input.rest.dto.response.TaskResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/tasks")
@RequiredArgsConstructor
public class TaskRestAPI {

    private final TaskService taskService;

    @PostMapping
    public ResponseEntity<TaskResponseDTO> createATask(@RequestBody TaskCreateDTO dto) {
        var model = dto.toModel();
        var savedTask = taskService.saveTask(model);
        return ResponseEntity.ok(new TaskResponseDTO(savedTask.getTitle(), savedTask.getTasks(), savedTask.isCompleted()));
    }
}
