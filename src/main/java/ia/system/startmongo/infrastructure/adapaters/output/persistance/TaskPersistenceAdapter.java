package ia.system.startmongo.infrastructure.adapaters.output.persistance;

import ia.system.startmongo.application.ports.output.TaskOutputPort;
import ia.system.startmongo.domain.models.Task;
import ia.system.startmongo.infrastructure.adapaters.output.persistance.mapper.TaskPersistenceMapper;
import ia.system.startmongo.infrastructure.adapaters.output.persistance.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class TaskPersistenceAdapter implements TaskOutputPort {

    private final TaskRepository taskRepository;
    private final TaskPersistenceMapper taskMapper;

    @Override
    public Task saveTask(Task task) {
        var entity = taskMapper.toEntity(task);
        var taskSaved = taskRepository.insert(entity);
        return taskMapper.toModel(taskSaved);
    }

    @Override
    public Optional<Task> getTask(String id) {
        return taskRepository.findById(id).map(taskMapper::toModel);
    }

    @Override
    public List<Task> getListByUserId(String userId) {
        return taskRepository.findAllByOwnerId(userId).stream()
            .map(taskMapper::toModel)
            .toList();
    }
}
