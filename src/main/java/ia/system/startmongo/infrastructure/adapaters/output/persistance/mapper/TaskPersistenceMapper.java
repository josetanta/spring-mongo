package ia.system.startmongo.infrastructure.adapaters.output.persistance.mapper;

import ia.system.startmongo.domain.models.Task;
import ia.system.startmongo.infrastructure.adapaters.output.persistance.entity.TaskEntity;
import org.springframework.stereotype.Component;

@Component
public class TaskPersistenceMapper implements PersistenceMapper<Task, TaskEntity> {

    @Override
    public TaskEntity toEntity(Task model) {
        return TaskEntity.builder()
            .id(model.getId())
            .completed(model.isCompleted())
            .title(model.getTitle())
            .tasks(model.getTasks())
            .ownerId(model.getOwnerId())
            .build();
    }

    @Override
    public Task toModel(TaskEntity entity) {
        return Task.builder()
            .id(entity.getId())
            .completed(entity.isCompleted())
            .title(entity.getTitle())
            .tasks(entity.getTasks())
            .ownerId(entity.getOwnerId())
            .build();
    }
}
