package ia.system.startmongo.infrastructure.adapaters.input.rest.dto.request;

import ia.system.startmongo.domain.models.Task;

import java.util.List;

public record TaskCreateDTO(
    String title,
    String ownerId,
    List<String> tasks,
    boolean completed
) {

    public Task toModel() {
        return Task.builder()
            .completed(completed)
            .ownerId(ownerId)
            .title(title)
            .tasks(tasks)
            .build();
    }
}
