package ia.system.startmongo.infrastructure.adapaters.input.rest;

import ia.system.startmongo.domain.models.User;
import ia.system.startmongo.domain.services.TaskService;
import ia.system.startmongo.domain.services.UserService;
import ia.system.startmongo.infrastructure.adapaters.input.rest.dto.response.TaskResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserRestAPI {

    private final UserService userService;
    private final TaskService taskService;

    @PostMapping
    public ResponseEntity<User> registerUser(@RequestBody User user) {
        return ResponseEntity.ok(userService.registerUser(user));
    }

    @GetMapping
    public ResponseEntity<List<TaskResponseDTO>> getAllTaskByUser(@RequestParam("user_id") String userId) {

        var tasks = taskService.getListByUserId(userId);
        var resp = tasks.stream()
            .map(t -> new TaskResponseDTO(t.getTitle(), t.getTasks(), t.isCompleted()))
            .toList();
        return ResponseEntity.ok(resp);
    }
}
