package ia.system.startmongo.infrastructure.adapaters.input.rest.dto.response;

import java.util.List;

public record TaskResponseDTO(
    String title,
    List<String> tasks,
    boolean completed
) {
}
