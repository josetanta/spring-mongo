package ia.system.startmongo.infrastructure.adapaters.output.persistance;

import ia.system.startmongo.application.ports.output.UserOutputPort;
import ia.system.startmongo.domain.models.User;
import ia.system.startmongo.infrastructure.adapaters.output.persistance.mapper.UserPersistenceMapper;
import ia.system.startmongo.infrastructure.adapaters.output.persistance.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserPersistenceAdapter implements UserOutputPort {

    private final UserRepository userRepository;
    private final UserPersistenceMapper userPersistenceMapper;

    @Override
    public User saveUser(User user) {
        var entity = userPersistenceMapper.toEntity(user);
        var savedUser = userRepository.save(entity);
        return userPersistenceMapper.toModel(savedUser);
    }

    @Override
    public Optional<User> getUser(String id) {
        var entity = userRepository
            .findById(id)
            .orElseThrow();

        var user = userPersistenceMapper.toModel(entity);
        return Optional.of(user);
    }
}
