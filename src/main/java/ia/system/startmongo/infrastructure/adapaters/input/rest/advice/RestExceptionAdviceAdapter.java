package ia.system.startmongo.infrastructure.adapaters.input.rest.advice;

import com.mongodb.MongoException;
import com.mongodb.MongoWriteException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

@RestControllerAdvice
public class RestExceptionAdviceAdapter {

    @ExceptionHandler(MongoWriteException.class)
    public ResponseEntity<Object> handleMongoWriteException(MongoException ex) {
        var errors = new HashMap<String, String>();

        if (ex instanceof MongoWriteException mEx) {
            errors.put("message", mEx.getMessage());
        }

        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(errors);
    }
}
