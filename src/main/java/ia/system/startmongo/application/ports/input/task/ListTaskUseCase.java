package ia.system.startmongo.application.ports.input.task;

import ia.system.startmongo.domain.models.Task;

import java.util.List;

public interface ListTaskUseCase {
    List<Task> getListByUserId(String userId);
}
