package ia.system.startmongo.application.ports.output;

import ia.system.startmongo.domain.models.Task;

import java.util.List;
import java.util.Optional;

public interface TaskOutputPort {
    Task saveTask(Task task);

    Optional<Task> getTask(String id);

    List<Task> getListByUserId(String userId);
}
