package ia.system.startmongo.application.ports.input.task;

import ia.system.startmongo.domain.models.Task;

public interface CreateTaskUseCase {
    Task saveTask(Task task);
}
