package ia.system.startmongo.application.ports.input.user;

import ia.system.startmongo.domain.models.User;

public interface RetrieveUserUseCase {
    User getUserById(String id);
}
