package ia.system.startmongo.application.ports.input.task;

import ia.system.startmongo.domain.models.Task;

import java.util.Optional;

public interface RetrieveTaskUseCase {
    Optional<Task> getTask(String id);
}
