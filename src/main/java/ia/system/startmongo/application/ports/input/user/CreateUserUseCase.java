package ia.system.startmongo.application.ports.input.user;

import ia.system.startmongo.domain.models.User;

public interface CreateUserUseCase {
    User registerUser(User user);
}
