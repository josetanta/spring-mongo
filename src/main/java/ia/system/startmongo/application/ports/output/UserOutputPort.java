package ia.system.startmongo.application.ports.output;

import ia.system.startmongo.domain.models.User;

import java.util.Optional;

public interface UserOutputPort {
    User saveUser(User user);

    Optional<User> getUser(String id);
}
