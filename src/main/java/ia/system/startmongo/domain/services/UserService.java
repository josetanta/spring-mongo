package ia.system.startmongo.domain.services;

import ia.system.startmongo.application.ports.input.user.CreateUserUseCase;
import ia.system.startmongo.application.ports.input.user.RetrieveUserUseCase;
import ia.system.startmongo.domain.models.User;
import ia.system.startmongo.infrastructure.adapaters.output.persistance.UserPersistenceAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService implements CreateUserUseCase, RetrieveUserUseCase {

    private final UserPersistenceAdapter userOutputPort;

    @Override
    public User registerUser(User user) {
        return userOutputPort.saveUser(user);
    }

    @Override
    public User getUserById(String id) {
        return userOutputPort.getUser(id).orElseThrow();
    }
}
