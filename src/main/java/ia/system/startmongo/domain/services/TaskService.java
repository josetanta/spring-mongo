package ia.system.startmongo.domain.services;

import ia.system.startmongo.application.ports.input.task.CreateTaskUseCase;
import ia.system.startmongo.application.ports.input.task.ListTaskUseCase;
import ia.system.startmongo.application.ports.input.task.RetrieveTaskUseCase;
import ia.system.startmongo.domain.models.Task;
import ia.system.startmongo.infrastructure.adapaters.output.persistance.TaskPersistenceAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TaskService implements CreateTaskUseCase, RetrieveTaskUseCase, ListTaskUseCase {

    private final TaskPersistenceAdapter persistenceAdapter;

    @Override
    public Task saveTask(Task task) {
        return persistenceAdapter.saveTask(task);
    }

    @Override
    public List<Task> getListByUserId(String userId) {
        return persistenceAdapter.getListByUserId(userId);
    }

    @Override
    public Optional<Task> getTask(String id) {
        return persistenceAdapter.getTask(id);
    }
}
